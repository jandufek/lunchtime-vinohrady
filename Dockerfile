FROM debian
RUN mkdir -p /usr/src/app
RUN mkdir -p /data/db
WORKDIR /usr/src/app

RUN apt-get update && apt-get install -y \
  make \
  mongodb \
  curl \ 
  openjdk-7-jre \ 
  nodejs \ 
  g++
  
COPY . /usr/src/app

RUN make install-dependencies
RUN make compile


CMD [ "make", "run" ]
